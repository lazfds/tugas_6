1. Buat Database:

	create Database nama_database
	
	MariaDB [(none)]> create database myshop;
	
	MariaDB [(none)]> show databases;
	
	MariaDB [(none)]> use myshop;


2. CREATE TABLE nama_tabel (variabel_column, tipe data, key, etc);

   a. Tabel users
	MariaDB [myshop]> CREATE TABLE users(
	    -> id_user int AUTO_INCREMENT PRIMARY KEY,
	    -> name varchar(255),
	    -> email varchar(255),
	    -> password varchar(255)
	    -> );

   b. Tabel categories
	MariaDB [myshop]> CREATE TABLE categories(
	    -> id_category int AUTO_INCREMENT PRIMARY KEY,
	    -> name varchar(255)
	    -> );

   c. Tabel items
	MariaDB [myshop]> CREATE TABLE items(
	    -> id_items int AUTO_INCREMENT,
	    -> name varchar(255),
	    -> description varchar(255),
	    -> price int,
	    -> stock int,
	    -> category_id int,
	    -> PRIMARY KEY (id_items),
	    -> FOREIGN KEY (category_id) REFERENCES categories(id_category));

	MariaDB [myshop]> show tables;
	+------------------+
	| Tables_in_myshop |
	+------------------+
	| categories       |
	| items            |
	| users            |
	+------------------+
	3 rows in set (0.001 sec)

3. INSERT INTO nama_table VALUES (column1, column2, ...);

	MariaDB [myshop]> describe categories;
	+-------------+--------------+------+-----+---------+----------------+
	| Field       | Type         | Null | Key | Default | Extra          |
	+-------------+--------------+------+-----+---------+----------------+
	| id_category | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name        | varchar(255) | YES  |     | NULL    |                |
	+-------------+--------------+------+-----+---------+----------------+
	2 rows in set (0.017 sec)

	MariaDB [myshop]> describe items
	    -> ;
	+-------------+--------------+------+-----+---------+----------------+
	| Field       | Type         | Null | Key | Default | Extra          |
	+-------------+--------------+------+-----+---------+----------------+
	| id_items    | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name        | varchar(255) | YES  |     | NULL    |                |
	| description | varchar(255) | YES  |     | NULL    |                |
	| price       | int(11)      | YES  |     | NULL    |                |
	| stock       | int(11)      | YES  |     | NULL    |                |
	| category_id | int(11)      | YES  | MUL | NULL    |                |
	+-------------+--------------+------+-----+---------+----------------+
	6 rows in set (0.019 sec)

	MariaDB [myshop]> describe users;
	+----------+--------------+------+-----+---------+----------------+
	| Field    | Type         | Null | Key | Default | Extra          |
	+----------+--------------+------+-----+---------+----------------+
	| id_user  | int(11)      | NO   | PRI | NULL    | auto_increment |
	| name     | varchar(255) | YES  |     | NULL    |                |
	| email    | varchar(255) | YES  |     | NULL    |                |
	| password | varchar(255) | YES  |     | NULL    |                |
	+----------+--------------+------+-----+---------+----------------+
	4 rows in set (0.020 sec)

	MariaDB [myshop]> INSERT INTO users (name, email, password) VALUES ('John Doe', 'john@doe.com', 'john123');
	Query OK, 1 row affected (0.003 sec)
	
	MariaDB [myshop]> SELECT * FROM users;
	+---------+----------+--------------+----------+
	| id_user | name     | email        | password |
	+---------+----------+--------------+----------+
	|       1 | John Doe | john@doe.com | john123  |
	+---------+----------+--------------+----------+
	1 row in set (0.000 sec)
	
	MariaDB [myshop]> INSERT INTO users (name, email, password) VALUES ('Jane Doe', 'jane@doe.com', 'jenita123');
	Query OK, 1 row affected (0.002 sec)
	
	MariaDB [myshop]> SELECT * FROM users;
	+---------+----------+--------------+-----------+
	| id_user | name     | email        | password  |
	+---------+----------+--------------+-----------+
	|       1 | John Doe | john@doe.com | john123   |
	|       2 | Jane Doe | jane@doe.com | jenita123 |
	+---------+----------+--------------+-----------+
	2 rows in set (0.000 sec)

	MariaDB [myshop]> INSERT INTO categories (name) VALUES ('Gadget'), ('cloth'), ('men'), ('women'),
	    -> ('branded');
	Query OK, 5 rows affected (0.006 sec)
	Records: 5  Duplicates: 0  Warnings: 0
	
	MariaDB [myshop]> select * from categories;
	+-------------+---------+
	| id_category | name    |
	+-------------+---------+
	|           1 | Gadget  |
	|           2 | cloth   |
	|           3 | men     |
	|           4 | women   |
	|           5 | branded |
	+-------------+---------+
	5 rows in set (0.000 sec)

	MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id) VALUES
	    -> ('Samsung B50', 'HP keren dari merek samsung', 4000000, 100, 1),
	    -> ('Uniklooh', 'Baju keren dari brand ternama', 500000, 50, 2),
	    -> ('IMHO Watch', 'Jam tangan anak yang jujur banget', 2000000, 10, 1);
	Query OK, 3 rows affected (0.052 sec)
	Records: 3  Duplicates: 0  Warnings: 0

	MariaDB [myshop]> SELECT * FROM items;
	+----------+-------------+-----------------------------------+---------+-------+-------------+
	| id_items | name        | description                       | price   | stock | category_id |
	+----------+-------------+-----------------------------------+---------+-------+-------------+
	|        1 | Samsung B50 | HP keren dari merek samsung       | 4000000 |   100 |           1 |
	|        2 | Uniklooh    | Baju keren dari brand ternama     |  500000 |    50 |           2 |
	|        3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
	+----------+-------------+-----------------------------------+---------+-------+-------------+
	3 rows in set (0.001 sec)

4. SELECT DATA (pilih data di dalam basis data berdasarkan tabel dan kolomnya):

   a. SELECT id_user, name, email FROM users;
	
	+---------+----------+--------------+
	| id_user | name     | email        |
	+---------+----------+--------------+
	|       1 | John Doe | john@doe.com |
	|       2 | Jane Doe | jane@doe.com |
	+---------+----------+--------------+
	2 rows in set (0.001 sec)

   b. > SELECT * FROM items WHERE price >= 1000000;
	
		+----------+-------------+-----------------------------------+---------+-------+-------------+
		| id_items | name        | description                       | price   | stock | category_id |
		+----------+-------------+-----------------------------------+---------+-------+-------------+
		|        1 | Samsung B50 | HP keren dari merek samsung       | 4000000 |   100 |           1 |
		|        3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
		+----------+-------------+-----------------------------------+---------+-------+-------------+
		2 rows in set (0.006 sec)
      
	> SELECT * FROM items WHERE name LIKE '%uniklo%';
		
		+----------+----------+-------------------------------+--------+-------+-------------+
		| id_items | name     | description                   | price  | stock | category_id |
		+----------+----------+-------------------------------+--------+-------+-------------+
		|        2 | Uniklooh | Baju keren dari brand ternama | 500000 |    50 |           2 |
		+----------+----------+-------------------------------+--------+-------+-------------+
		1 row in set (0.001 sec)


   c. Alternatif jawaban saya:

	> ALTER TABLE categories CHANGE name category_name varchar(255);
	MariaDB [myshop]> ALTER TABLE categories CHANGE name category_name varchar(255);
	Query OK, 0 rows affected (0.012 sec)
	Records: 0  Duplicates: 0  Warnings: 0

	MariaDB [myshop]> describe categories;
	+---------------+--------------+------+-----+---------+----------------+
	| Field         | Type         | Null | Key | Default | Extra          |
	+---------------+--------------+------+-----+---------+----------------+
	| id_category   | int(11)      | NO   | PRI | NULL    | auto_increment |
	| category_name | varchar(255) | YES  |     | NULL    |                |
	+---------------+--------------+------+-----+---------+----------------+
	2 rows in set (0.011 sec)

	> SELECT items.name, items.description, items.price, items.stock,
	items.category_id, categories.category_name (tidak digunakan: as kategori)
	FROM items INNER JOIN categories
	ON items.category_id = categories.id_category;

	MariaDB [myshop]> SELECT items.name, items.description, items.price, items.stock,
	items.category_id, categories.category_name
	FROM items INNER JOIN categories
	ON items.category_id = categories.id_category;
	+-------------+-----------------------------------+---------+-------+-------------+---------------+
	| name        | description                       | price   | stock | category_id | category_name |
	+-------------+-----------------------------------+---------+-------+-------------+---------------+
	| Samsung B50 | HP keren dari merek samsung       | 4000000 |   100 |           1 | Gadget        |
	| Uniklooh    | Baju keren dari brand ternama     |  500000 |    50 |           2 | cloth         |
	| IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 | Gadget        |
	+-------------+-----------------------------------+---------+-------+-------------+---------------+
	3 rows in set (0.001 sec)


5. 	MariaDB [myshop]> UPDATE items SET price = 2500000 WHERE id_items = 1;
	Query OK, 1 row affected (0.009 sec)
	Rows matched: 1  Changed: 1  Warnings: 0

	MariaDB [myshop]> SELECT * FROM items;
	+----------+-------------+-----------------------------------+---------+-------+-------------+
	| id_items | name        | description                       | price   | stock | category_id |
	+----------+-------------+-----------------------------------+---------+-------+-------------+
	|        1 | Samsung B50 | HP keren dari merek samsung       | 2500000 |   100 |           1 |
	|        2 | Uniklooh    | Baju keren dari brand ternama     |  500000 |    50 |           2 |
	|        3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
	+----------+-------------+-----------------------------------+---------+-------+-------------+
	3 rows in set (0.000 sec)

.....