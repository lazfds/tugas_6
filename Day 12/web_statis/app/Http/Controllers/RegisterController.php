<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //asdasd
    public function form(){
        return view('register');
    }

    // Request get (Mengambil data dari url dan metode get)
    // Data form akan muncul jika menggunakan request get
    // public function welcome_get(Request $request){
    //     var_dump($request);
    //     dd($request["]);
    //     return view('register');
    // }

    //Menggunakan Request request untuk mengambil data jika dibutuhkan
    //Request post mengambil data dari forum secara langsung
    //Fungsi Request $request digunakan untuk parsing data (mengoper data)
    public function welcome(Request $request){
        // dd($request->all());
        $nama_depan = $request["nama_depan"];
        $nama_belakang = $request["nama_belakang"];
        //array setelah bagian halaman (welcome) akan digunakan
        //untuk melakukan parsing data
        //menggunakan array asosiatif
        //nilai tanda kutip '' yang terdapat pada array tersebut
        //akan digunakan pada halaman register.blade.php
        //lalu dapat dipanggilkan dengan tanda $variabel sebagai variabel pemanggilan data yang baru
        return view('welcome', ['nama_depan' => $nama_depan, 'nama_belakang' => $nama_belakang]);
    }
}
