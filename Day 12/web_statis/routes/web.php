<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/test/{angka}', function ($angka) {
    //Menggunakan array asosiatif
    return view('test', ["angka" => $angka]);
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/halo/{nama}', function ($nama) {
    return "halo $nama";
});

//menggunakan tanda @ (at) untuk memanggil function yang ada di folder
//app/Http/Controller
Route::get("/register", 'RegisterController@form');
Route::post("/welcome_get", 'RegisterController@welcome_get');
Route::post("/welcome", 'RegisterController@welcome');
