<?php

class Ape extends Animal
{
    protected $legs = 2;
    protected $teriak = "Auooo";

    //override class dari class Animal
    //nilai $legs otomatis berubah menjadi 2
    public function get_legs()
    {
        return $this->legs;
    }

    public function yell()
    {
        return $this->teriak;
    }
}
