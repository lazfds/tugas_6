<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <?php

    require('Animal.php');
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");

    //reminder
    
    /*
    $this pada Class
    Di dalam class kita akan sering menulis $this yang berarti merujuk kepada class itu sendiri.
    scope atau ruang lingkup $this adalah segala sesuatu yang ada di dalam tanda kurung
    kurawal { } setelah penamaan class NamaClass.
    */

     
    /*
    Tips: karena menggunakan sintaks yang kompleks (menggunakan pemanggilan objek function), maka pemanggilan
    echo nya dari kelas animal, frog, dan ape harus menggunakan {}

    referensi
    http://www.phpknowhow.com/basics/usage-of-brackets/
    https://stackoverflow.com/questions/2596837/curly-braces-in-string-in-php
    https://www.php.net/manual/en/language.types.string.php#language.types.string.parsing.complex
    https://schoolsofweb.com/php-curly-braces-how-and-when-to-use-it/
    */

    echo "Nama: {$sheep->get_name()} <br>"; // "shaun"
    echo "Jumlah kaki: {$sheep->get_legs()} <br>"; // 4
    echo "Berdarah dingin: {$sheep->get_type()} <br>"; // "no"

    echo "<br><br>";
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    // index.php
    $kodok = new Frog("buduk");
    echo "Nama: {$kodok->get_name()} <br>"; // "shaun"
    echo "Jumlah kaki: {$kodok->get_legs()} <br>"; // 4
    echo "Berdarah dingin: {$kodok->get_type()} <br>"; // "no"
    echo "Lompat: {$kodok->jump()} <br>"; // "auooo"

    echo "<br><br>";

    // index.php
    $sungokong = new Ape("kera sakti");
    echo "Nama: {$sungokong->get_name()} <br>"; // "shaun"
    echo "Jumlah kaki: {$sungokong->get_legs()} <br>"; // 4
    echo "Berdarah dingin: {$sungokong->get_type()} <br>"; // "no"
    echo "Teriak: {$sungokong->yell()} <br>"; // "auooo"

    ?>

</body>

</html>