@extends('admin.master')

{{-- judul halaman parsial admin - wrapper --}}
@section('page_title')
    Data Pemeran
@endsection

{{-- judul items parsial admin - wrapper --}}
@section('items_title')
    Buat data pemeran
@endsection

@section('content')
    <div class="ml-3 mt-3">
        <div class="card card-primary">

            <form role="form" action="/cast" method="POST">

                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama"
                            placeholder="Masukkan nama pemeran" value="{{ old('nama', '') }}" required>
                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur"
                            placeholder="Masukkan umur pemeran" value="{{ old('umur', '') }}" required>
                        @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea class="form-control" id="bio" rows="4" name="bio" style="resize:none;" required></textarea>
                        {{-- @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror --}}
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
