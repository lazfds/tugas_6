@extends('admin.master')

{{-- judul halaman parsial admin - wrapper --}}
@section('page_title')
    Data Pemeran
@endsection

{{-- judul items parsial admin - wrapper --}}
@section('items_title')
    Edit data pemeran nomor {{$cast->id}}
@endsection

@section('content')
    <div class="ml-3 mt-3">
        <div class="card card-primary">

            <form role="form" action="/cast/{{$cast->id}}" method="POST">

                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama pemeran"
                            value="{{ old('nama', $cast->nama) }}" required>
                    </div>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="text" class="form-control" id="umur" name="umur" placeholder="Masukkan umur pemeran"
                            value="{{ old('umur', $cast->umur) }}" required>
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea class="form-control" id="bio" rows="4" name="bio" style="resize:none;"
                            required>{{{ old('bio', $cast->bio) }}}</textarea>
                        {{-- @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror --}}
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
