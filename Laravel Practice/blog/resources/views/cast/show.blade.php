@extends('admin.master')

{{-- judul halaman parsial admin - wrapper --}}
@section('page_title')
    Show data pemeran
@endsection

{{-- judul items parsial admin - wrapper --}}
@section('items_title')
    Daftar konten data pemeran
@endsection

@section('content')
    <div class="mt-3 ml-3">
        <h3> {{ $cast->nama }} </h4>
        <br>
        <p><b>{{ $cast->umur }} tahun</b></p>
        <p> {{ $cast->bio }} </p>
    </div>
@endsection
